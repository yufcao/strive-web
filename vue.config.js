module.exports = {
	// 公共路径配置
	// publicPath: '/strive/app/modules/strive-web',
	publicPath: '/strive-web',
	// 服务器配置
	devServer: {
		port: 8081
	},
	// webpack链式配置
	chainWebpack: config => {
		config.module
			.rule('iView')
			.test(/\.vue$/)
			.use()
			.loader('vue-loader')
			.loader('iview-loader')
			.options({
				prefix: false
			}).end()
	},
	// webpack配置
	configureWebpack: {
		resolve: {
			alias: {
				'vue$': 'vue/dist/vue.esm.js'
			}
		}
	},
};
