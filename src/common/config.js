import Vue from 'vue'

const config = {
	loginUrl: 'http://localhost:9090/strive', // 单点登录url
	baseUrl: 'http://localhost:9090/strive', // 统一url前缀
	tokenName: 'strive_token', // localStorage保存token的名称
	userInfoName: 'strive_user_info', // localStorage保存用户信息的item名称
	rolePrefix: 'ROLE_' // springsecurity角色前缀
}

export default config

Vue.prototype.$config = config